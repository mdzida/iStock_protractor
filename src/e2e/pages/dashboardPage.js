let DashboardPage = function() {

    this.accountButton = $('.actions-list').$('.wide-header');
    this.signOutButton = element(by.id("hypSignOut"));
    this.tablesButton = element(by.id('open_board'));
    this.seeAllTablesButton = element(by.className('view-board-link'));

    this.picturesButton = $$('[data-nav="nav=nav_Photos"]').first();
    this.ilustrationButton = $$('[data-nav="nav=nav_Illustrations"]').first();
    this.videoButton = $$('[data-nav="nav=nav_Video"]').first();
    this.soundsButton = $$('[data-nav="nav=nav_Audio"]').first();

    this.goToSubPage = (subPage) => {
        subPage.click();
    };

    this.goToAccountSlide = () => {
        this.accountButton.click();
    };

    this.goToMyBoards = () => {
        browser.actions().mouseMove(this.tablesButton).perform();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(this.seeAllTablesButton));
        this.seeAllTablesButton.click();
    };

    this.logout = () => {
        this.goToAccountSlide();
        this.signOutButton.click();
    };
};

module.exports = DashboardPage;