let MyBoardPage = function () {

    this.myTablesField = $('.board-management').element(by.tagName('h2'));
    this.createBoardButton = $('[ng-click="showCreateBoardModal()"]');

    this.numOfBoards = $('.my-boards-tab').$('.board-count');
    this.boardNameInput = element(by.model('vm.newBoardName'));
    this.createBoardPopup = $('[ng-click="vm.createNewBoard()"');

    this.boardNameField =  $$('.board-summary').first().$('.board-name');
    this.deleteBoard = $$('[ng-click="delete(board)"]').get(1);
       
    this.renameBoardButton = $('.board-name').$('[ng-click="editBoardName(board)"]');
    this.renameBoardInput = element(by.model('board.name'));

    this.createNewBoard = (boardName) => {
        this.createBoardButton.click();
        this.boardNameInput.sendKeys(boardName);
        this.createBoardPopup.click();   
    };

    this.deleteNewBoard = () => {
        this.deleteBoard.click();
        browser.switchTo().alert().accept();
    };

    this.renameBoardName = (newName) => {
        this.renameBoardButton.click();
        this.renameBoardInput.clear().sendKeys(newName);
        this.renameBoardInput.sendKeys(protractor.Key.ENTER);
    };
};

module.exports = MyBoardPage;