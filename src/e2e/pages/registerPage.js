let RegisterPage = function () {

    this.alreadyMemberField = element(by.id("already-a-member"));
    this.emailInput = element(by.model("register['email']"));
    this.passwordInput = element(by.model("register['password']"));
    this.passwordConfirmationInput = element(by.model("register['passwordConfirmation']"));

    this.alertEmailMessage = element.all(by.tagName('ng-message')).first();
    this.alertPasswordMessage = element.all(by.tagName('ng-message')).get(1);

    this.registerButton = element(by.id("register-button"));

    this.clearInputFields = () => {
        this.emailInput.clear();
        this.passwordInput.clear();
        this.passwordConfirmationInput.clear();
    };

    this.fillInputWithData = (dataInputType, invalidData) => {
        dataInputType.sendKeys(invalidData);
    };

    this.register = () => {
        this.registerButton.click();
    };

};

module.exports = RegisterPage;