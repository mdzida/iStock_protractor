let LoginPage = function () {
    this.usernameInput = element(by.model("new_session['username']"));
    this.passwordInput = element(by.model("new_session['password']"));
    this.signInButton = element(by.id("sign_in"));

    this.invalidInputPopup = element(by.className("alert_box"));

    this.clearInputFields = () => {
        this.usernameInput.clear();
        this.passwordInput.clear();
    };

    this.fillUserData = (username, password) => {
        this.usernameInput.sendKeys(username);
        this.passwordInput.sendKeys(password);
    };

    this.login = () => {
        return this.signInButton.click();
    };
}

module.exports = LoginPage;