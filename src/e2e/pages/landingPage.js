let LandingPage = function() {

    this.header = $$('.headline').first().element(by.tagName('h1'));
    this.loginButton = element(by.xpath('//*[@id="site-header"]/div[2]/nav[3]/div/ul/li[5]/a'));

    this.registerButton = $$('[data-nav="nav=nav_Join"]').first();
   
    this.goToLoginPage = () => this.loginButton.click();

    this.goToRegisterPage = () => this.registerButton.click();
};

module.exports = LandingPage;