let LoginPage = require('../pages/loginPage');
let DashboardPage = require('../pages/dashboardPage');

let LoginLogoutProvider = function () {
    this.loginPage = new LoginPage();
    this.dashboardPage = new DashboardPage();

    this.login = () => {
        this.loginPage.clearInputFields();
        this.loginPage.fillUserData(browser.params.username, browser.params.password);
        this.loginPage.login();
    };

    this.logout = () => {
        this.dashboardPage.logout();
    };
};

module.exports = LoginLogoutProvider;