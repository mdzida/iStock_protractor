let openPageProvider = function() {
    this.openPage = () => {
        browser.get(browser.params.baseUrl);
    };
};

module.exports = openPageProvider;