const localConfig = require('./config/local.json');
const mainRegularFlowTestsPath = 'suites/regular-flow/';
const mainNegativeFlowTestsPath = 'suites/negative-flow/';
const seleniumJar = require('./node_modules/selenium-server-standalone-jar');

exports.config = {

    framework: 'jasmine',
    seleniumServerJar: seleniumJar.path,
    rootElement: 'body',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ["--window-size=1200,900" ]
        }
    },
    jasmineNodeOpts: {
        defaultTimeoutInterval: 25000
    },

    specs: [
        mainRegularFlowTestsPath + 'login.spec.js',
        mainNegativeFlowTestsPath + 'loginRefusal.spec.js',
        mainRegularFlowTestsPath + 'register.spec.js',
        mainNegativeFlowTestsPath + 'registerRefusal.spec.js',
        mainRegularFlowTestsPath + 'myBoard.spec.js',
        mainRegularFlowTestsPath + 'dashboard.spec.js'
    ],

    baseUrl: localConfig.baseUrl,
    params: localConfig
};