const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const LandingPage = require('../../pages/landingPage');
const LoginPage = require('../../pages/loginPage');

describe("Verify impossibility of login...", () => {
    let landingPage = new LandingPage();
    let loginPage = new LoginPage();
    let openBasePageProvider = new OpenBasePageProvider();

    beforeAll(() => {
        openBasePageProvider.openPage();
    });

    it("should not be able to login with incorrect username and password", () => {
        landingPage.goToLoginPage().then(() => {
            loginPage.fillUserData(browser.params.incorrectUsername, browser.params.incorrectPassword);
        });
        loginPage.login().then(() => {
            expect(loginPage.invalidInputPopup.getText()).toContain("Sprawdź, czy dane");
        });
    });

    it("should not be able to login with no data", () => {
        loginPage.clearInputFields();
        loginPage.login().then(() => {
            expect(landingPage.header.isPresent()).toBe(false);
        });
    });

    it('should not be able to login with inverted data', () => {
        loginPage.clearInputFields();
        loginPage.fillUserData(browser.params.password, browser.params.username);
        loginPage.login().then(() => {
            expect(landingPage.header.isPresent()).toBe(false);
        });
    });
});