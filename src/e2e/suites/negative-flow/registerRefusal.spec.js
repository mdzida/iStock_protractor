const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const LandingPage = require('../../pages/landingPage');
const RegisterPage = require('../../pages/registerPage');

describe('Verify if registration is not available when...', () => {
    let openBasePageProvider = new OpenBasePageProvider();
    let landingPage = new LandingPage();
    let registerPage = new RegisterPage();

    beforeAll( () => {
        openBasePageProvider.openPage();
    });

    afterEach( () => {
        registerPage.clearInputFields();
    });

    it('Should not be able to register with no data', () => {
        landingPage.goToRegisterPage().then(() => {
            registerPage.clearInputFields();
        });
        expect(registerPage.alertEmailMessage.getText()).toContain("Wprowadź wymagane informacje");
    });

    it('Should not be able to register with invalid e-mail', () => {
        registerPage.fillInputWithData(registerPage.emailInput, browser.params.invalidEmail);
        registerPage.register();
        expect(registerPage.alertEmailMessage.getText()).toContain("Prosimy o wpisanie adresu w prawidłowym formacie");
    });

    it('Should not be able to register when password and confrimation password differ', () => {
        registerPage.fillInputWithData(registerPage.passwordInput, browser.params.incorrectPassword);
        registerPage.fillInputWithData(registerPage.passwordConfirmationInput, browser.params.invalidConfPassword);
        registerPage.register();
        expect(registerPage.alertPasswordMessage.getText()).toContain("Hasło nie pasuje");
    });

    it('Should not be able to register with weak password - less than 8 letters', () => {
        registerPage.fillInputWithData(registerPage.passwordInput, browser.params.passwordWithLessThanEightLetters);
        registerPage.register();
        expect(registerPage.alertPasswordMessage.getText()).toContain("Hasła muszą składać się z 8");
    });

    it('Should not be able to register with weak password - no digits', () => {
        registerPage.fillInputWithData(registerPage.passwordInput, browser.params.passwordWithNoDigit);
        registerPage.register();
        expect(registerPage.alertPasswordMessage.getText()).toContain("jedną cyfrę");
    });

    it('Should not be able to register with weak password - no letters', () => {
        registerPage.fillInputWithData(registerPage.passwordInput, browser.params.passwordWithNoLetter);
        registerPage.register();
        expect(registerPage.alertPasswordMessage.getText()).toContain("jedną literę");
    });

    it('Should not be able to register with passwords which differ in only one capital letter', () => {
        registerPage.fillInputWithData(registerPage.passwordInput, browser.params.incorrectPassword);
        registerPage.fillInputWithData(registerPage.passwordConfirmationInput, browser.params.passwordWithCapitalLetter);
        registerPage.register();
        expect(registerPage.alertPasswordMessage.getText()).toContain("Hasło nie pasuje");
    });
});