const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const LandingPage = require('../../pages/landingPage');
const LoginPage = require('../../pages/loginPage');
const DashboardPage = require('../../pages/dashboardPage');

describe("Verify that login and logout are possible...", () => {
    let landingPage = new LandingPage();
    let loginPage = new LoginPage();
    let dashboardPage = new DashboardPage();
    let openBasePageProvider = new OpenBasePageProvider();

    beforeAll( () => {
        openBasePageProvider.openPage();
    });

    it("should be on the landing page", () => {
        expect(landingPage.header.getText()).toContain("Mniej szukania");
    });

    it("should be able to enter login page", () => {
        landingPage.goToLoginPage().then( () => {
            expect(loginPage.usernameInput.isDisplayed()).toBe(true);
        });
    });

    it("should be able to enter valid user data and login", () => {
        loginPage.fillUserData(browser.params.username, browser.params.password);
        loginPage.login().then( () => { 
            expect(dashboardPage.accountButton.isDisplayed()).toBe(true);
        });
    });

    it("should be able to logout", () => {
        dashboardPage.logout();
        expect(landingPage.loginButton.isDisplayed()).toBe(true);
    });

});