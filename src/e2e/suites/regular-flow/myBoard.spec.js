const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const LoginLogoutProvider = require('../../utils/loginLogoutProvider');
const MyBoardPage = require('../../pages/myBoardPage');
const DashboardPage = require('../../pages/dashboardPage');
const LandingPage = require('../../pages/landingPage');

describe('Verify possibilities of adding new boards...', () => {
    let openBasePageProvider = new OpenBasePageProvider();
    let loginLogoutProvider = new LoginLogoutProvider();
    let myBoardPage = new MyBoardPage();
    let dashboardPage = new DashboardPage();
    let landingPage = new LandingPage();

    beforeAll(() => {
        openBasePageProvider.openPage();
        landingPage.goToLoginPage();
        loginLogoutProvider.login();
    });

    it('Should be able to enter -my board- section', () => {
        dashboardPage.goToMyBoards();
        expect(myBoardPage.myTablesField.getText()).toContain("Moje tablice");
    });

    it('Should be able to load a board', () => {
        expect(myBoardPage.numOfBoards.getText()).toEqual("1");
    });

    it('Should be able to create new board', () => {
        myBoardPage.createNewBoard(browser.params.firstBoard);
        expect(myBoardPage.boardNameField.getAttribute('title')).toEqual(browser.params.firstBoard);
    });

    it('Should be able to delete recently created board', () => {
        myBoardPage.deleteNewBoard();
        expect(myBoardPage.numOfBoards.getText()).toEqual("1");
    });

    it('Should be able to rename a board', () => {
        myBoardPage.renameBoardName("fourthBoard");
        expect(myBoardPage.boardNameField.getAttribute('title')).toEqual("fourthBoard");
        myBoardPage.renameBoardName(browser.params.secondBoard);
    });

    it("should be able to logout", () => {
        dashboardPage.goToAccountSlide();
        loginLogoutProvider.logout();
    });

});