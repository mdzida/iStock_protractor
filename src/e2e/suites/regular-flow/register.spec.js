const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const LandingPage = require('../../pages/landingPage');
const RegisterPage = require('../../pages/registerPage');

describe("Verify that enter register page is possible... ", () => {
    let openBasePageProvider = new OpenBasePageProvider();    
    let landingPage = new LandingPage();
    let registerPage = new RegisterPage();

    beforeAll(() => {
        openBasePageProvider.openPage();
    });

    it("should be able to go to the register page", () => {
        landingPage.goToRegisterPage().then(() => {
            expect(registerPage.alreadyMemberField.getText()).toContain("Już jesteś zarejestrowanym");
        });
    });
});