const OpenBasePageProvider = require('../../utils/openBasePageProvider');
const DashboardPage = require('../../pages/dashboardPage');
const SubPages = require('../../pages/subPages');

describe("Verify if links lead to the right places", () => {
    let openBasePageProvider = new OpenBasePageProvider();
    let dashboardPage = new DashboardPage();
    let subPages = new SubPages();

    beforeAll(() => {
        openBasePageProvider.openPage();
    });

    it("Should be able to go to the 'pictures' page", () => {
        dashboardPage.goToSubPage(dashboardPage.picturesButton);
        expect(subPages.pictureFilterLabel.getText()).toEqual("Zdjęcia");
    });

    it("Should be able to go to the 'ilustration' page", () => {
        dashboardPage.goToSubPage(dashboardPage.ilustrationButton);
        expect(subPages.ilustrationsFilterLabel.getText()).toEqual("Ilustracje");
    });

    it("Should be able to go to the 'video' page", () => {
        dashboardPage.goToSubPage(dashboardPage.videoButton);
        expect(subPages.videoFilterLabel.getText()).toEqual("Wideo");
    });

    it("Should be able to go to the 'sounds' page", () => {
        dashboardPage.goToSubPage(dashboardPage.soundsButton);
        expect(subPages.soundsFilterLabel.getText()).toEqual("Obrazy");
    });
});