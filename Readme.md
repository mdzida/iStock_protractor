# iStock - Protractor: end-to-end tests
### Description
Protractor e2e tests were mede for iStock website. The website (iStock) was chosen due to its AngularJS structure.  Tests were carried only for several pages, including: landing page, dashboard page, login/logout, register page and a few subpages. The main attention was given to login/logout tests.  

### Requirements
In order to run tests locally one need to have installed: 

- Node.js - check whether you have it installed using command: 

		`node --version` 

example output: `Version 9.4.0`
- Java Development Kit (JDK), check with: 

		`java -version`

example output: `Version 1.8.0`
- Protractor, check with:

		`protractor --version`

 example output: `Version 5.2.2`

 - Selenium Server Standalone, install locally with:

    `npm install selenium-server-standalone-jar`

### How to run
Run protractor. Make sure it is run in this same directory as conf.js file. Use command:

			`protractor conf.js`

